## Main
General thoughts of this approach:
  - everything must be explicit, complexity for user can be handled by an UI application
  - no infers, no assumptions
  - core handles asset when it's explicit to do so
  - inputs/outputs are referenced by a unique id (`uid` key). The `uid` can be specified in the inputs/outputs definition, if not it will be composed by `<asset name>.<message name>`



| File | Description
| -- | --
| [protocol.yaml](protocol.yaml) | Configuration of a protocol app, interfacing with an RTU
| [model.yaml](model.yaml) | Configuration of a model app, getting data from the protocol app
| [bundle.yaml](bundle.yaml) | Configuration of a bundle containing both apps above
